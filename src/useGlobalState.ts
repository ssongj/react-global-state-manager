import {useState, useEffect, useCallback} from 'react';


const useGlobalState = (GSM, state) => {
    const update = useState()[1];

    const listener = useCallback( () => {
        update(Object.create(null));
    }, [update]);

    useEffect(() => {
        GSM.subscribe(state, listener);

        return () => GSM.unsubscribe(state, listener);
    }, []);


    return GSM.getState(state);
};

export default useGlobalState;