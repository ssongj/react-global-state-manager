const  addState = (GSM, key, value) => {
    GSM.addState({[key]: value});
    return GSM.getState(key);
};

export default addState;