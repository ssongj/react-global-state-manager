export const compareState = (state1, state2, compareFn=undefined) => {
    const keys = Object.keys(state1);
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        if (compareFn ? !compareFn(state1[key], state2[key]) : state1[key] !== state2[key]) {
            return false;
        }
    }
    return true;
};