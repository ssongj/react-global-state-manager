const  useAction = (GSM, name) => {
    return GSM.getAction(name);
};

export default useAction;