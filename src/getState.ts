const  getState = (GSM, state = undefined) => {
    return GSM.getState(state);
};

export default getState;