import { compareState } from './utils/compareState';

type Dispatch = () => void;
type Action = (...args: any[]) => void | {};
type SideEffect = (...args: any[]) => {};

class StateManager<
      S extends {}
    > {
  private _state: S;
  private _actions: Map<string, (...args: any[]) => void | {}>;
  private _dispatchers: Map<keyof S, Set<Dispatch>>;
  private _sideEffects: Map<keyof S, Set<SideEffect>>;

  public constructor (
      initialState: S = Object.create(null),
  ) {
    this._state = Object.assign({}, initialState);
    this._actions = new Map();
    this._dispatchers = new Map();
    this._sideEffects = new Map();
  }

  // Initialize state and reducer in the beginning
  public init (
      state: S = Object.create(null)
  ): S {
    this._state = Object.assign({}, this._state, state);
    return this._state;
  }

  private _compareStates(
      newState: S
  ): boolean {
    const newKeys = Object.keys(newState);
    for (let i = 0; i < newKeys.length; i++) {
      if (this._state[newKeys[i]]) {
        return true;
      }
    }
    return false;
  }

  public getState (
      state: keyof S = undefined
  ): S | S[keyof S] {
    if (state && this._state[state] === undefined) {
      throw 'This state does not exist in GSM'
    }
    return state ? this._state[state] : this._state;
  }

  public addState (
      newState: S
  ): void {
    if (this._compareStates(newState)) {
      throw 'given state already exists in the system.'
    }
    this._state = Object.assign({}, this._state, newState);
  }

  public addSideEffect (
      sideEffect: SideEffect,
      ...states: (keyof S)[]
  ): void {
    for (let i = 0; i < states.length; i++) {
      const state = states[i]
      if (this._sideEffects.has(state)) {
        this._sideEffects.get(state).add(sideEffect);
      } else {
        this._sideEffects.set(state, new Set([sideEffect]));
      }
    }
  }

  public subscribe(
      state: keyof S,
      dispatcher: Dispatch
  ): void {
    if (this._dispatchers.has(state)) {
      this._dispatchers.get(state).add(dispatcher);
    } else {
      this._dispatchers.set(state, new Set([dispatcher]));
    }
  }

  public unsubscribe(
      state: keyof S,
      dispatcher: Dispatch
  ): void {
    this._dispatchers.get(state).delete(dispatcher);
  }

  public getAction(
      name: string
  ): Action {
    return this._actions.get(name);
  }

  public addAction(
      name: string,
      action: (S, ...args:any[]) => Partial<S>
  ): void {
    this._actions.set(
      name,
      async (...args: any[]) => {
        // Perform action and receive actions
        let newState = await action(this._state, ...args);
        // no need to update if new state did not change.
        if (compareState(newState, this._state)) return;
        this._state = Object.assign({}, this._state, newState);
        // run side effects
        const dispatchers = this.runSideEffects(newState);
        // loop through dispatch list and get affected state keys
        // then perform update on associated containers
        this.dispatch(dispatchers);
      }
    )
  }

  public dispatch(
      state: Partial<S>
  ): void {
    for (let key in state) {
      if (state.hasOwnProperty(key)) {
        const dispatchers = this._dispatchers.get(key);
        if (!dispatchers) continue;
        dispatchers.forEach(forceUpdate => {
          forceUpdate();
        })
      }
    }
  }

  public runSideEffects(
      state: Partial<S>
  ): Partial<S> {
    let currentState = state;
    for (let key in state) {
      if (state.hasOwnProperty(key)) {
        const sideEffects = this._sideEffects.get(key);
        if (!sideEffects) continue;
        sideEffects.forEach(effect => {
          const result = effect(this._state);
          this._state = Object.assign({}, this._state, result);
          currentState = Object.assign({}, currentState, result);
        })
      }
    }
    return currentState;
  }
}

export default StateManager;
