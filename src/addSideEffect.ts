const  addSideEffect = (GSM, sideEffect, ...state) => {
    GSM.addSideEffect(sideEffect, ...state);
};

export default addSideEffect;