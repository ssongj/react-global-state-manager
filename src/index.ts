import gsm from './globalState';
import init from './init';
import addAction from './addAction';
import useAction from './useAction';
import addState from './addState';
import getState from './getState';
import addSideEffect from './addSideEffect';
import useGlobalState from './useGlobalState';

export = {
    GlobalStateManager: init.bind(null, gsm),
    addAction: addAction.bind(null, gsm),
    useAction: useAction.bind(null, gsm),
    addState: addState.bind(null, gsm),
    getState: getState.bind(null, gsm),
    addSideEffect: addSideEffect.bind(null, gsm),
    useGlobalState: useGlobalState.bind(null, gsm),
};