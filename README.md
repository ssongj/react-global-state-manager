# Global State Manager

Create global state manager utilizing React Hooks API. (No Redux!)

## Install
- `npm install react-global-state-manager`

## Usage

#### GlobalStateManager

Use `GlobalStateManager` to initiate global state manger for your app.
```
import React from 'react';
import ReactDOM from 'react-dom';
import { GlobalStateManager } from 'react-global-state-manager';

const App = () => {
    // Create Global State Manager 
    const state = GlobalStateManager({ apple: 5, orange: 10 });
    return (
    <>
        <h1>Fruit Vendor</h1>
        <table>
            <tr>
                <th>Fruit</th>
                <th>Cost</th>
            </tr>
            <tr>
                <td>Apple</td>
                <td>{state.apple}</td>
            </tr>
            <tr>
                <td>Orange</td>
                <td>{state.orange}</td>
            </tr>
        </table>
    </> 
    );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
```

#### useGlobalState
 
`useGlobalState` allows you to grab specific state in GSM and subscribe your container to listen to the changes to the requested state.

```
...

const Apple = () => {
  const price = useGlobalState('apple');

  return (
    <tr>
      <td>Apple</td>
      <td>{ price }</td>
    </tr>
  )
}

const Orange = () => {
  const price = useGlobalState('orange');

  return (
    <tr>
      <td>Orange</td>
      <td>{ price }</td>
    </tr>
  )
}

const Fruit = () => {
  const state = GlobalStateManager({ apple: 5, orange: 10 });
  return (
    <>
      <h1>Fruit Vendor</h1>
      <table>
        <tr>
          <th>Fruite</th>
          <th>Cost</th>
        </tr>
        <tbody>
          <Apple />
        </tbody>
        <tbody>
          <Orange />
        </tbody>
      </table>
    </>
  )
}

...
```

#### addAction
Create a new action which will trigger state update.
```
...

const Fruit = () => {
  const state = GlobalStateManager({ apple: 5, orange: 10 });

  addAction('changeApple', (state, newPrice) => {
    return { apple: newPrice };
  });

  return (
    <>
      <h1>Fruit Vendor</h1>
      <table>
        <tr>
          <th>Fruite</th>
          <th>Cost</th>
        </tr>
        <tbody>
          <Apple />
        </tbody>
        <tbody>
          <Orange />
        </tbody>
      </table>
    </>
  )
}

...
```
#### useAction
Get a named action from GSM. Performing this action will trigger all components subscribed to this state to be re-rendered.
```
...

const Apple = () => {
  const [newPrice, setNewPrice] = useState('');
  const price = useGlobalState('apple');
  const changePrice = getAction('changeOrange');
  return (
    <>
      <tr>
        <td>Apple</td>
        <td>{ price }</td>
      </tr>
      <div style={{'position': 'aboslute', 'right': '0'}}>
        <input type='text' value={newPrice} placeholder='New Price' onChange={(e) => setNewPrice(e.target.value)} />
        <button onClick={()=>changePrice(parseInt(newPrice))}>Change</button>
      </div>
    </>
  )
}

const Orange = () => {
  const [newPrice, setNewPrice] = useState('');
  const price = useGlobalState('orange');
  const changePrice = getAction('changeApple');

  return (
    <>
      <tr>
        <td>Orange</td>
        <td>{ price }</td>
      </tr>
      <div style={{'position': 'aboslute', 'right': '0'}}>
        <input type='text' value={newPrice} placeholder='New Price' onChange={(e) => setNewPrice(e.target.value)} />
        <button onClick={()=>changePrice(parseInt(newPrice))}>Change</button>
      </div>
    </>
  )
}

const Fruit = () => {
  const state = GlobalStateManager({ apple: 5, orange: 10 });

  addAction('changeApple', (state, newPrice) => {
    return { apple: newPrice };
  });

  addAction('changeOrange', (state, newPrice) => {
    return { orange: newPrice };
  });

  return (
    <>
      <h1>Fruit Vendor</h1>
      <table>
        <tr>
          <th>Fruite</th>
          <th>Cost</th>
        </tr>
        <tbody>
          <Apple />
        </tbody>
        <tbody>
          <Orange />
        </tbody>
      </table>
    </>
  )
}

...
```
In this example, apple vendor can change the price of an orange and orange vendor can change the price of an apple.

#### addSideEffect
Introduce side effect to be ran when desired state is being updated.
```
  ...
  
  const Fruit = () => {
    const state = GlobalStateManager({ apple: 5, orange: 10 });
    
    ...
    // set side effect for apple.
    addSideEffect((state) => {
      console.log('You can't mess with me! I add 5 more!');
      return { apple: state.apple + 5}
    }, 'apple');
    
    // set side effect for multiple states.
    addSideEffect((state) => {
      console.log('Price changed in fruits');
      return state
    }, 'apple', 'orange');
    
    ...
    
  }
```
- Top effect will increase price of an apple by 5 whenever orange vendor tries to change the price of an apple.
- You can also set one side effect for multiple states.
#### addState (WIP)
In progress. `addState` will add a state from anywhere in your app, and will subscribe current container to listen to the changes.
You can only use newly added state from children of current container.
If you try to `useGlobalState` from any other container, it will throw an error.